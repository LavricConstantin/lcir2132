package salariati.main;

import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.repository.mock.EmployeeMock;
import salariati.validator.EmployeeValidator;
import salariati.controller.EmployeeController;
import salariati.enumeration.DidacticFunction;

import java.util.ArrayList;

//functionalitati
//F01.	 adaugarea unui nou angajat (nume, prenume, CNP, functia didactica, salariul de incadrare);
//F02.	 modificarea functiei didactice (asistent/lector/conferentiar/profesor) a unui angajat;
//F03.	 afisarea salariatilor ordonati descrescator dupa salariu si crescator dupa varsta (CNP).

public class StartApp {
	
	public static void main(String[] args) {
		
		EmployeeRepositoryInterface employeesRepository = new EmployeeImpl();
		EmployeeController employeeController = new EmployeeController(employeesRepository);

		ArrayList<Employee> a = (ArrayList<Employee>) employeeController.getEmployeesList();

		for(Employee _employee : employeeController.getEmployeesList())
			System.out.println(_employee.toString());
		System.out.println("-----------------------------------------");

		Employee employee = new Employee("FirstName","LastName", "1234567894321", DidacticFunction.ASISTENT, "2500");
		employeeController.addEmployee(employee);


		for(Employee _employee : employeeController.getEmployeesList())
			System.out.println(_employee.toString());

		System.out.println("-----------------------------------------");

		Employee employee2 = new Employee("FirstName","LastName", "1234567894321", DidacticFunction.CONF, "2500");
		employeeController.modifyEmployee(employee,employee2);


		for(Employee _employee : employeeController.getEmployeesList())
			System.out.println(_employee.toString());

	EmployeeValidator validator = new EmployeeValidator();
		System.out.println( validator.isValid(new Employee("FirstName","LastName", "1234567894322", DidacticFunction.TEACHER, "3400")) );
//
	//	System.out.println(validator.isValid(new Employee("333","33333","1234567890123",DidacticFunction.TEACHER,"3000")));

	}

}

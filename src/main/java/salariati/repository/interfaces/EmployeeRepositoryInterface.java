package salariati.repository.interfaces;

import java.util.List;
import salariati.model.Employee;
import salariati.validator.EmployeeValidator;

public interface EmployeeRepositoryInterface {
	//getEmployeeList nu e pe diag
	//adaugat chestia de jos
	List<Employee> employeeList = null;
	EmployeeValidator employeeValidator = null;
	
	boolean addEmployee(Employee employee);
	void deleteEmployee(Employee employee);
	void modifyEmployee(Employee oldEmployee, Employee newEmployee);
	List<Employee> getEmployeeList();
	List<Employee> getEmployeeByCriteria(String criteria);

}

package salariati.repository.implementations;

import java.io.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import salariati.exception.EmployeeException;

import salariati.model.Employee;

import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeImpl implements EmployeeRepositoryInterface {
	//astea 2 nu.s pe diag + getEmployeeList function
	private final String employeeDBFile = "src/main/prof.txt";
	//"employeeDB/employees.txt";
	private EmployeeValidator employeeValidator;
//add
    List<Employee> employeeList = new ArrayList<Employee>() {};

    //add Construnctor
	public int length(){
		return employeeList.size();
	}


	public EmployeeImpl(){
		 this.employeeValidator = new EmployeeValidator();
		this.employeeList = getEmployeeList();
	}
	@Override
	public boolean addEmployee(Employee employee) {
		if( employeeValidator.isValid(employee) ) {
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, true));
				bw.newLine();
				bw.write(employee.toString());
				//bw.newLine();
				bw.close();

				employeeList.add(employee);
				return true;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return false;
	}

	@Override
	public void deleteEmployee(Employee employee) {
		// TODO Auto-generated method stub
		
	}
	public void setModify(Employee oldEmployee, Employee newEmployee){
		oldEmployee.setFunction(newEmployee.getFunction());
		oldEmployee.setSalary(newEmployee.getSalary());
	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
		if( employeeValidator.isValid(newEmployee) ) {
			setModify(oldEmployee,newEmployee);
			BufferedWriter bw = null;
			try {
				bw = new BufferedWriter(new FileWriter(employeeDBFile, false));
				for(Employee e:employeeList){

				//bw.newLine();
				bw.write(e.toString());
				bw.newLine();
				}
				bw.close();
				employeeList.add(newEmployee);

			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		
	}

	@Override
	public List<Employee> getEmployeeList() {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(employeeDBFile));
			String line;
			int counter = 0;
			while ((line = br.readLine()) != null) {
				Employee employee = new Employee();
				try {//addif
					if(line!=null){
					employee = (Employee) Employee.getEmployeeFromString(line, counter);
					employeeList.add(employee);}
				} catch(EmployeeException ex) {
					System.err.println("Error while reading: " + ex.toString());
				}
			}
		} catch (FileNotFoundException e) {
			System.err.println("Error while reading: " + e);
		} catch (IOException e) {
			System.err.println("Error while reading: " + e);
		} finally {
			if (br != null)
				try {
					br.close();
				} catch (IOException e) {
					System.err.println("Error while closing the file: " + e);
				}
		}
		
		return sort(employeeList);
	}

	public List<Employee> sort(List<Employee> list){
		ArrayList<Employee> sortedList = (ArrayList<Employee>) list;

		return sortedList.stream().sorted(Comparator.comparing(Employee::getSalaryLong).reversed().thenComparing(Employee::getCnp)).parallel().collect(Collectors.toList());

	}

//nu facenimic
	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		List<Employee> employeeList = new ArrayList<Employee>();
		
		return employeeList;
	}

}

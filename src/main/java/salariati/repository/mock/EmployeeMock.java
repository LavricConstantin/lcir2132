package salariati.repository.mock;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import salariati.enumeration.DidacticFunction;

import salariati.model.Employee;
import salariati.repository.interfaces.EmployeeRepositoryInterface;
import salariati.validator.EmployeeValidator;

public class EmployeeMock implements EmployeeRepositoryInterface {
	// list<> em... nu e pe diag + funct getEmployeeList nu e pe diag
	private List<Employee> employeeList;
	private EmployeeValidator employeeValidator;
	
	public EmployeeMock() {
		
		employeeValidator = new EmployeeValidator();
		employeeList = new ArrayList<Employee>();
		
//		Employee Ionel   = new Employee("Andrei","Pacuraru", "1234567890879", DidacticFunction.ASISTENT, "1500");
//		Employee Mihai   = new Employee("Andrei","Dumitrescu", "1234567890877", DidacticFunction.LECTURER, "500");
//		Employee Ionela  = new Employee("Andrei","Ionescu", "1234567890876", DidacticFunction.LECTURER, "4500");
//		Employee Mihaela = new Employee("Andrei","Pacuraru", "1234567890876", DidacticFunction.ASISTENT, "5500");
//		Employee Vasile  = new Employee("Andrei","Georgescu", "1234567890879", DidacticFunction.TEACHER,  "2500");
//		Employee Marin   = new Employee("Andrei","Puscas", "1234567890876", DidacticFunction.TEACHER,  "2500");
//
//		employeeList.add( Ionel );
//		employeeList.add( Mihai );
//		employeeList.add( Ionela );
//		employeeList.add( Mihaela );
//		employeeList.add( Vasile );
//		employeeList.add( Marin );
	}

	public int lenght(){
		return employeeList.size();
	}

	@Override
	public boolean addEmployee(Employee employee) {
		if ( employeeValidator.isValid(employee)) {
			employeeList.add(employee);
			return true;
		}
		return false;
	}
	
	@Override
	public void deleteEmployee(Employee employee) {
			employeeList.remove(employee);

	}

	@Override
	public void modifyEmployee(Employee oldEmployee, Employee newEmployee) {
//implemtentation
		if(employeeValidator.isValid(newEmployee)){
			oldEmployee.setFunction(newEmployee.getFunction());
			oldEmployee.setSalary(newEmployee.getSalary());
		}
	}

	@Override
	public List<Employee> getEmployeeList() {
		return (List<Employee>) sort();
	//return employeeList;
	}


	//adaugare sort si adaugare getSalaryLong in Model
	public List<Employee> sort(){
//		//List<Employee> list = employeeList.sort(Comparator.comparing(Employee::getSalary).reversed().thenComparing(Employee::getCnp));
//				//employeeList.sort(Comparator.comparing(Employee::get))
//
//		Comparator<Employee> combinedComparator = Comparator.comparing(Employee::getSalary).reversed()
//				.thenComparing(Employee::getCnp);
//		//List<Employee> emppArr = new ArrayList<Employee>();
//		Employee[] emppArr = employeeList.toArray(new Employee[empss.size()]);
//
////Parallel sorting
//		Arrays.parallelSort(new List[]{emppArr}, combinedComparator);

	ArrayList<Employee> sortedList = (ArrayList<Employee>) employeeList;

		return employeeList.stream().sorted(Comparator.comparing(Employee::getSalaryLong).reversed().thenComparing(Employee::getCnp)).parallel().collect(Collectors.toList());

}

	@Override
	public List<Employee> getEmployeeByCriteria(String criteria) {
		// TODO Auto-generated method stub
		return null;
	}

}

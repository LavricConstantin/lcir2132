package salariati.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;

import static org.junit.Assert.*;

public class EmployeeTest {

    private Employee e1;
    private Employee e2;



    @Before
    public void setUp() throws Exception {
        e1 = new Employee("Gigel","Frone","1234567890123", DidacticFunction.TEACHER,"2000");
        e2 = new Employee("Ffff","asdasa","1234567890123",DidacticFunction.CONF,"5000");
    }

    @After
    public void tearDown() throws Exception {
        e1 = null;
        e2 = null;
    }

    @Test
    public void getLastName() {
        assertEquals("Frone",e1.getLastName());
    }

    @Test
    public void getFunction() {

        assertEquals(DidacticFunction.CONF,e2.getFunction());
    }

    @Test
    public void getSalary() {
        assertEquals("5000",e2.getSalary());
    }
}
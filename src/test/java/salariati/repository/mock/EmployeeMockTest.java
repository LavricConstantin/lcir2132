package salariati.repository.mock;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;
import salariati.repository.implementations.EmployeeImpl;

import static org.junit.Assert.*;

public class EmployeeMockTest {
    EmployeeMock repo;
    private int repSize;
    @Before
    public void setUp() throws Exception {
        repo = new EmployeeMock();
        repSize = repo.lenght();
        addTest();
    }

    @After
    public void tearDown() throws Exception{
    }



    public void addTest(){
        Employee e1 = new Employee("Gigel","Frone","1234567890123", DidacticFunction.TEACHER,"2000");
        repSize++;
        repo.addEmployee(e1);
        Employee e2 = new Employee("Ffff","asdasa","1234567890123",DidacticFunction.CONF,"5000");
        repSize++;
        repo.addEmployee(e2);
        assertEquals(repo.lenght(),repSize);
    }

    @Test
    public void addTestECPValid1(){
        Employee e = new Employee("fffff","ddddd","1234567890123",DidacticFunction.CONF,"300");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize+1);
    }
    @Test
    public void addTestECPValid2(){
        Employee e = new Employee("ffffffffffffffffffffffffffffff","ddddd","1234567890123",DidacticFunction.CONF,"300");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize+1);
    }


//    @Test
//    public void addTestEcpInvalid1(){//salariu <0
//        Employee e = new Employee("fffff","ddddd","1234567890123",DidacticFunction.CONF,"-1");
//        repo.addEmployee(e);
//        assertEquals(repo.lenght(),repSize);
//    }

    @Test
    public void addTestEcpInvalid2(){//salriu nu e numar
        Employee e = new Employee("fffff","ddddd","1234567890123",DidacticFunction.CONF,"ff");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize);
    }
    @Test
    public void addTestEcpInvalid3(){//firstName nu e string
        Employee e = new Employee("1234","ddddd","1234567890123",DidacticFunction.CONF,"1234");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize);
    }




    @Test
    public void addTestBVAValid1(){//lim inf nume
        Employee e = new Employee("fff","ddddd","1234567890123",DidacticFunction.CONF,"300");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize+1);
    }

    @Test
    public void addTestBVAValid2(){//lim sup nume
        Employee e = new Employee("ffffffffffffffffffffffffffffff","ddddd","1234567890123",DidacticFunction.CONF,"300");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize+1);
    }
    @Test
    public void addTestBVAInvalid1(){//lim sup+1 nume
        Employee e = new Employee("fffffffffffffffffffffffffffffff","ddddd","1234567890123",DidacticFunction.CONF,"300");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize);
    }

    @Test
    public void addTestBVAInvalid2(){//lim inf nume
        Employee e = new Employee("ff","ddddd","1234567890123",DidacticFunction.CONF,"300");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize);
    }

    @Test
    public void addTestBVAValid3(){//salariu 1
        Employee e = new Employee("fff","ddddd","1234567890123",DidacticFunction.CONF,"1");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize+1);
    }

    @Test
    public void addTestBVAInvalid3(){//salariu <0
        Employee e = new Employee("fff","ddddd","1234567890123",DidacticFunction.CONF,"-1");
        repo.addEmployee(e);
        assertEquals(repo.lenght(),repSize);
    }

//    @Test
//    public void addTestBVAInvalid4(){//salariu string
//        Employee e = new Employee("fff","ddddd","1234567890123",DidacticFunction.CONF,"fff");
//        repo.addEmployee(e);
//        assertEquals(repo.lenght(),repSize);
//    }




}

//lastName lenght >3 <30  false <3
//salariu int si >0 false pt <0 si nu eint
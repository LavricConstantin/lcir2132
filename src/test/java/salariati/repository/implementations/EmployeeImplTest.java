package salariati.repository.implementations;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import salariati.enumeration.DidacticFunction;
import salariati.model.Employee;

import static org.junit.Assert.*;

public class EmployeeImplTest {
    EmployeeImpl repo;
    private int repSize;
    @Before
    public void setUp() throws Exception {
        repo = new EmployeeImpl();
        repSize = 4;
    }

    @After
    public void tearDown() throws Exception{
    }


    @Test
    public void addTest(){
       Employee e1 = new Employee("Gigel","Frone","1234567890123", DidacticFunction.TEACHER,"2000");
       repSize++;
       repo.addEmployee(e1);
       Employee e2 = new Employee("Ffff","asdasa","1234567890123",DidacticFunction.CONF,"5000");
       repSize++;
       repo.addEmployee(e2);
       assertEquals(repo.length(),repSize);
    }

    @Test
    public void addTestECPValid(){}

}